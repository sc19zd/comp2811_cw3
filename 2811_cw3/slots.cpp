#include "slots.h"
#include "ui_setup.h"
#include <iostream>
#include "the_player.h"
#include <QWidget>
#include <QVideoWidget>
#include <QObject>
#include <iostream>

void Slots::make_connections(){
    // clicking the next button will change the currently playing video to the next video in the list
    connect(ui->nextVideo_button, SIGNAL(clicked()), mplayer, SLOT(next()));

    // clicking the previous button will change the currently playing video to the previous video viewed
    connect(ui->previousVideo_button, SIGNAL(clicked()), mplayer,SLOT(previous()));

    // selecting an option in the combo box to change the video playback rate will do so
    connect(ui->changeRate_button,SIGNAL(activated(int)), SLOT(updateRate()));
}

// returns the currently selected option in the combo box used to change the video playback rate
qreal Slots::playbackRate() const {
    return (qreal) changeRate_button->itemData(changeRate_button->currentIndex()).toDouble();
}

// changes the video playback rate
void Slots::setRateUpdate(qreal playbackRate) {
    qint64 currentPosition = mplayer->position();
    mplayer->setPlaybackRate(playbackRate);
    mplayer->setPosition(currentPosition);
}

void Slots::updateRate() {
        emit changeRate(playbackRate());
}


