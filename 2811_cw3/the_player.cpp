//
// Created by twak on 11/11/2019.
//

#include "the_player.h"
#include <iostream>

using namespace std;

// all buttons have been setup, store pointers here
void ThePlayer::setContent(std::vector<TheButton*>* b, std::vector<TheButtonInfo>* i) {
    buttons = b;

    for (int a=0; a< (int) buttons->size();a++) {
        buttons->at(a)->info->index = a;
    }

    infos = i;
    jumpTo(buttons -> at(0) -> info);
}

// change the image and video for one button every one second
void ThePlayer::shuffle() {
    TheButtonInfo* i = & infos -> at (updateCount++ % infos->size() );
//        setMedia(*i->url);
    buttons -> at( updateCount++ % buttons->size() ) -> init( i );
}

void ThePlayer::playStateChanged (QMediaPlayer::State ms) {
    switch (ms) {
        case QMediaPlayer::State::StoppedState:
            play(); // starting playing again...
            break;
    default:
        break;
    }
}

void ThePlayer::jumpTo (TheButtonInfo* button) {
    // Updates the values of the curr and prev attributes appropriately
    prev = curr;
    curr = button->index;

    setMedia( * button -> url);
    play();
}

// Sets the current position in the video to the given value
void ThePlayer::set_pos (int pos) {
    this->setPosition(((qint64) pos));
}

// Switches to the next video in the list
void ThePlayer::next() {
    // Calculates the position of the next video in the list
    int next = (curr+1)%(buttons->size());

    // Sets the ThePlayer's media to this video
    setMedia( * buttons->at(next)->info->url);

    // Updates the values of the curr and prev attributes accordingly
    prev = curr;
    curr = next;

    play();
}

// Switches to the video that was previously viewed
void ThePlayer::previous() {
    // Sets the ThePlayer's media to this video
    setMedia( * buttons->at(prev)->info->url);

    // Updates the values of the curr and prev attributes accordingly
    int temp = prev;
    prev = curr;
    curr = temp;

    play();
}
