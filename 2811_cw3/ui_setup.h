#ifndef UI_SETUP_H
#define UI_SETUP_H


/********************************************************************************
** Form generated from reading UI file 'test.ui'
**
** Created by: Qt User Interface Compiler version 5.15.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/
#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLCDNumber>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QSlider>
#include <QtWidgets/QWidget>
#include <iostream>

QT_BEGIN_NAMESPACE

class Ui_Setup
{
public:
    QWidget *gridLayoutWidget;
    QGridLayout *gridLayout;
    QPushButton *previousVideo_button;
    QPushButton *nextVideo_button;
    QComboBox *changeRate_button;
    QLabel *rate_label;

   //Setup the widgets used in the Ui for cycle 3
   void setupUi(QWidget *Form) {
        if (Form->objectName().isEmpty())
            Form->setObjectName(QString::fromUtf8("Form"));

        // Setup the form
        Form->resize(800, 110);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(Form->sizePolicy().hasHeightForWidth());
        Form->setSizePolicy(sizePolicy);
        Form->setMinimumSize(QSize(780, 110));
        Form->setMaximumSize(QSize(10000, 110));

        //Set up the layout
        gridLayoutWidget = new QWidget(Form);
        gridLayoutWidget->setObjectName(QString::fromUtf8("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(-1, -1, 801, 101));
        gridLayout = new QGridLayout(gridLayoutWidget);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setSizeConstraint(QLayout::SetMaximumSize);
        gridLayout->setContentsMargins(0, 0, 0, 0);

        // Set up the previous button
        previousVideo_button= new QPushButton(gridLayoutWidget);
        previousVideo_button->setObjectName(QString::fromUtf8("previous_video"));
        previousVideo_button->setEnabled(true);
        previousVideo_button->setMinimumSize(QSize(20, 20));

        // Set up the next button
        nextVideo_button= new QPushButton(gridLayoutWidget);
        nextVideo_button->setObjectName(QString::fromUtf8("next_video"));
        nextVideo_button->setEnabled(true);
        nextVideo_button->setMinimumSize(QSize(20, 20));

        // Set up the video playback rate label
        rate_label = new QLabel(gridLayoutWidget);
        rate_label->setObjectName(QString::fromUtf8("rate"));
        rate_label->setMinimumSize(QSize(20, 20));

        // Set up the Combo Box used to change the video playback rate
        changeRate_button = new QComboBox();
        changeRate_button->setObjectName(QString::fromUtf8("change_rate"));
        changeRate_button->addItem("0.25x", QVariant(0.25));
        changeRate_button->addItem("0.5x", QVariant(0.5));
        changeRate_button->addItem("0.75x", QVariant(0.75));
        changeRate_button->addItem("1.0x", QVariant(1.0));
        changeRate_button->addItem("2.0x", QVariant(2.0));
        changeRate_button->setCurrentIndex(3);
        changeRate_button->setMinimumSize(QSize(40, 20));

        // Add the widgets to the layout
        gridLayout->addWidget(previousVideo_button, 1, 1, 1, 1);
        gridLayout->addWidget(nextVideo_button, 1, 2, 1, 1);
        gridLayout->addWidget(rate_label,1,3,1,1);
        gridLayout->addWidget(changeRate_button,1,4,1,1);

        retranslateUi(Form);

        QMetaObject::connectSlotsByName(Form);
    } // setupUi

    void retranslateUi(QWidget *Form) {
        Form->setWindowTitle(QCoreApplication::translate("Form", "Form", nullptr));
        nextVideo_button->setText(QCoreApplication::translate("Form", "Next", nullptr));
        previousVideo_button->setText(QCoreApplication::translate("Form", "Previous", nullptr));
        rate_label->setText(QCoreApplication::translate("Form", "Change Rate", nullptr));
    } // retranslateUi

};


namespace Ui {
    class Form: public Ui_Setup {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SETUP_H
