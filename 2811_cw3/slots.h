#ifndef SLOTS_H
#define SLOTS_H

#include "ui_setup.h"
#include "the_player.h"
#include "the_button.h"
#include <QWidget>
#include <QVideoWidget>
#include <QObject>
#include <iostream>

class Slots : public QWidget, Ui::Form {

    Q_OBJECT

public:
    Slots(ThePlayer *player) {
        ui = new Ui::Form();
        mplayer = player;
        ui->setupUi(this);

        make_connections();
    }
    qreal playbackRate() const;
    Ui::Form* getUi(){
        return this->ui;
    }

    void make_connections();

    // set the playback rate to the updated value
    void setRateUpdate(qreal playbackRate);

    ~Slots() {
        delete ui;
    }
public slots:
    // update the playback rate of the video
    void updateRate();

signals:
    // change the playback rate of the video
    void changeRate(qreal rate);

    // switch to the next video in the list
    void next();

    // switch to the video that was previously playing
    void previous();

private:
    Ui::Form *ui;

    // Holds a pointer to the ThePlayer shown on the main screen
    ThePlayer *mplayer;
};



#endif // SLOTS_H
