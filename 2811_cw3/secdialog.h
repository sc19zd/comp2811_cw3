#ifndef SECDIALOG_H
#define SECDIALOG_H

#include"QDialog"
#include"QHBoxLayout"
#include "QPushButton"
#include "QLabel"
#include "QPixmap"

class QPushButton;

class Sedialog : public QDialog
{
public:
    // the two buttons for the second dialog window
    QPushButton *p;
    QPushButton *p2;

    // create the widgets shown in the dialog window
    void createwidget();

    // arrange the widgets shown in the dialog window
    void arrangewidget();

    // set up the second dialog window
    Sedialog() {
        createwidget();
        arrangewidget();
        show();
    }

private:
    QLabel *c;
    QLabel *z;
};

#endif // SECDIALOG_H
