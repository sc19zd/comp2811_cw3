/****************************************************************************
** Meta object code from reading C++ file 'cycle1.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.15.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../cycle1.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'cycle1.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.15.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Cycle1_t {
    QByteArrayData data[9];
    char stringdata0[118];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Cycle1_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Cycle1_t qt_meta_stringdata_Cycle1 = {
    {
QT_MOC_LITERAL(0, 0, 6), // "Cycle1"
QT_MOC_LITERAL(1, 7, 11), // "setup_slide"
QT_MOC_LITERAL(2, 19, 0), // ""
QT_MOC_LITERAL(3, 20, 16), // "change_slide_pos"
QT_MOC_LITERAL(4, 37, 20), // "setup_duration_label"
QT_MOC_LITERAL(5, 58, 17), // "change_time_label"
QT_MOC_LITERAL(6, 76, 10), // "play_pause"
QT_MOC_LITERAL(7, 87, 16), // "reset_play_pause"
QT_MOC_LITERAL(8, 104, 13) // "QMediaContent"

    },
    "Cycle1\0setup_slide\0\0change_slide_pos\0"
    "setup_duration_label\0change_time_label\0"
    "play_pause\0reset_play_pause\0QMediaContent"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Cycle1[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   44,    2, 0x0a /* Public */,
       3,    1,   47,    2, 0x0a /* Public */,
       4,    1,   50,    2, 0x0a /* Public */,
       5,    1,   53,    2, 0x0a /* Public */,
       6,    0,   56,    2, 0x0a /* Public */,
       7,    1,   57,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void, QMetaType::LongLong,    2,
    QMetaType::Void, QMetaType::LongLong,    2,
    QMetaType::Void, QMetaType::LongLong,    2,
    QMetaType::Void, QMetaType::LongLong,    2,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 8,    2,

       0        // eod
};

void Cycle1::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<Cycle1 *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->setup_slide((*reinterpret_cast< qint64(*)>(_a[1]))); break;
        case 1: _t->change_slide_pos((*reinterpret_cast< qint64(*)>(_a[1]))); break;
        case 2: _t->setup_duration_label((*reinterpret_cast< qint64(*)>(_a[1]))); break;
        case 3: _t->change_time_label((*reinterpret_cast< qint64(*)>(_a[1]))); break;
        case 4: _t->play_pause(); break;
        case 5: _t->reset_play_pause((*reinterpret_cast< QMediaContent(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 5:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QMediaContent >(); break;
            }
            break;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject Cycle1::staticMetaObject = { {
    QMetaObject::SuperData::link<QWidget::staticMetaObject>(),
    qt_meta_stringdata_Cycle1.data,
    qt_meta_data_Cycle1,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Cycle1::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Cycle1::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Cycle1.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "Ui::Cycle1_Form"))
        return static_cast< Ui::Cycle1_Form*>(this);
    return QWidget::qt_metacast(_clname);
}

int Cycle1::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
