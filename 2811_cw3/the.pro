QT += core gui widgets multimedia multimediawidgets

CONFIG += c++11


# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        cycle1.cpp \
        secdialog.cpp \
        slots.cpp \
        the_button.cpp \
        the_player.cpp \
        thirddialog.cpp \
        tomeo.cpp \
        ui_setup.cpp

HEADERS += \
    cycle1.h \
    secdialog.h \
    slots.h \
    the_button.h \
    the_player.h \
    thirddialog.h \
    ui_setup.h

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

FORMS += \
    test.ui

