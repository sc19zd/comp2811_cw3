#include "thirddialog.h"

vector<TheButtonInfo> getInfoIn () {

    vector<TheButtonInfo> out =  vector<TheButtonInfo>();
    QDir dir(QString(QCoreApplication::arguments().at(1)));
    QDirIterator it(dir);

    while (it.hasNext()) { // for all files

        QString f = it.next();

            if (f.contains("."))

#if defined(_WIN32)
            if (f.contains(".wmv"))  { // windows
#else
            if (f.contains(".mp4") || f.contains("MOV"))  { // mac/linux
#endif

            QString thumb = f.left( f .length() - 4) +".png";
            if (QFile(thumb).exists()) { // if a png thumbnail exists
                QImageReader *imageReader = new QImageReader(thumb);
                    QImage sprite = imageReader->read(); // read the thumbnail
                    if (!sprite.isNull()) {
                        QIcon* ico = new QIcon(QPixmap::fromImage(sprite)); // voodoo to create an icon for the button
                        QUrl* url = new QUrl(QUrl::fromLocalFile( f )); // convert the file location to a generic url
                        out . push_back(TheButtonInfo( url , ico  ) ); // add to the output list
                    }
                    else
                        qDebug() << "warning: skipping video because I couldn't process thumbnail " << thumb << Qt::endl;
            }
            else
                qDebug() << "warning: skipping video because I couldn't find thumbnail " << thumb << Qt::endl;
        }
    }

    return out;
}

// Create the widgets for the third dialog window
void Thirddialog::createwidget() {
    // Sets up the button for selecting to change the brightness
    p4 = new QToolButton();
    p4->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    p4->setIcon(QIcon(":/Change.png"));
    p4->setText("Brightness");
    p4->setFont(QFont("Times", 10, QFont::Bold));
    p4->setIconSize(QSize(30, 30));
    p4->setFixedSize(90,90);

    // Sets up the button for selecting to change the hue
    p5 = new QToolButton();
    p5->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    p5->setIcon(QIcon(":/hue.png"));
    p5->setText("Hue");
    p5->setFont(QFont("Times", 10, QFont::Bold));
    p5->setIconSize(QSize(30, 30));
    p5->setFixedSize(90,90);

    // Sets up the button for selecting to change the contrast
    p6 = new QToolButton();
    p6->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    p6->setIcon(QIcon(":/Contrast.png"));
    p6->setText("Contrast");
    p6->setFont(QFont("Times", 10, QFont::Bold));
    p6->setIconSize(QSize(30, 30));
    p6->setFixedSize(90,90);

    // Sets up the button for quitting the application
    p3 = new QPushButton("Quit");
    p3->setFont(QFont("Times", 10, QFont::Bold));

    // Sets up the slider for changing the brightness
    brightnessSlider = new QSlider();
    brightnessSlider = new QSlider(Qt::Horizontal);
    brightnessSlider->setRange(0, 128);
    brightnessSlider->setTickPosition(QSlider::TicksBelow);
    brightnessSlider->setTickInterval(10);
    brightnessSlider->setValue(0);

    // Sets up the slider for changing the brightness
    contrastSlider = new QSlider();
    contrastSlider = new QSlider(Qt::Horizontal);
    contrastSlider->setRange(0, 128);
    contrastSlider->setTickPosition(QSlider::TicksBelow);
    contrastSlider->setTickInterval(10);
    contrastSlider->setValue(0);

    // Sets up the slider for changing the hue
    hueSlider = new QSlider();
    hueSlider = new QSlider(Qt::Horizontal);
    hueSlider->setRange(0, 128);
    hueSlider->setTickPosition(QSlider::TicksBelow);
    hueSlider->setTickInterval(10);
    hueSlider->setValue(0);
}

void Thirddialog::arrangewidget()
{
    // collect all the videos in the folder
    videos = getInfoIn();

    // the widget that will show the video
    videoWidget = new QVideoWidget;

    // the QMediaPlayer which controls the playback
    player = new ThePlayer;
    player->setVideoOutput(videoWidget);

    // a row of buttons
    buttonWidget = new QWidget();

    // the buttons are arranged horizontally
    QHBoxLayout *layout1 = new QHBoxLayout();
    buttonWidget->setLayout(layout1);


    // create the four buttons
    for ( int i = 0; i < 4; i++ ) {
        TheButton *button = new TheButton(buttonWidget);
        button->connect(button, SIGNAL(jumpTo(TheButtonInfo* )), player, SLOT (jumpTo(TheButtonInfo* ))); // when clicked, tell the player to play.
        buttons.push_back(button);
        layout1->addWidget(button);
        button->init(&videos.at(i));
    }

    // tell the player what buttons and videos are available
    player->setContent(&buttons, & videos);

    // Create the layout object
    QGridLayout *layout = new QGridLayout();

    // Create the group box
    QGroupBox *box = new QGroupBox("Tool:");
    box->setFont(QFont("Times", 10, QFont::Bold));
    layout->addWidget(box,0,0);

    // Create the layout and add the buttons, for selecting which attribute to change, to it
    QVBoxLayout *boxlayout = new QVBoxLayout(box);
    boxlayout->addWidget(p4);
    boxlayout->addWidget(p6);
    boxlayout->addWidget(p5);

    //Add the video player widget and the 3 sliders for changing the attributes
    layout->addWidget(videoWidget,0,2);
    layout->addWidget(p3, 1, 0);
    layout->addWidget(brightnessSlider, 1, 2);
    layout->addWidget(contrastSlider, 1, 2);
    layout->addWidget(hueSlider, 1, 2);
    brightnessSlider->hide();
    contrastSlider->hide();
    hueSlider->hide();

    // Set up the window
    setWindowTitle("Video Editor");
    setLayout(layout);
    setFixedSize(800,500);
}

void Thirddialog::makeconnection()
{
    QObject::connect(this->p4, SIGNAL(clicked()), this->brightnessSlider, SLOT(show()));
    QObject::connect(this->p4, SIGNAL(clicked()), this->contrastSlider, SLOT(hide()));
    QObject::connect(this->p4, SIGNAL(clicked()), this->hueSlider, SLOT(hide()));
    QObject::connect(brightnessSlider, SIGNAL(sliderMoved(int)), videoWidget, SLOT(setBrightness(int)));
    QObject::connect(videoWidget, SIGNAL(brightnessChanged(int)), brightnessSlider, SLOT(setValue(int)));

    QObject::connect(this->p6, SIGNAL(clicked()), this->contrastSlider, SLOT(show()));
    QObject::connect(this->p6, SIGNAL(clicked()), this->brightnessSlider, SLOT(hide()));
    QObject::connect(this->p6, SIGNAL(clicked()), this->hueSlider, SLOT(hide()));
    QObject::connect(contrastSlider, SIGNAL(sliderMoved(int)), videoWidget, SLOT(setContrast(int)));
    QObject::connect(videoWidget, SIGNAL(contrastChanged(int)), contrastSlider, SLOT(setValue(int)));

    QObject::connect(this->p5, SIGNAL(clicked()), this->hueSlider, SLOT(show()));
    QObject::connect(this->p5, SIGNAL(clicked()), this->brightnessSlider, SLOT(hide()));
    QObject::connect(this->p5, SIGNAL(clicked()), this->contrastSlider, SLOT(hide()));
    QObject::connect(hueSlider, SIGNAL(sliderMoved(int)), videoWidget,SLOT(setHue(int)));
    QObject::connect(videoWidget, SIGNAL(hueChanged(int)), hueSlider,SLOT(setValue(int)));
}


