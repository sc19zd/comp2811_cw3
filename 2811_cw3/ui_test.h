/********************************************************************************
** Form generated from reading UI file 'test.ui'
**
** Created by: Qt User Interface Compiler version 5.15.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TEST_H
#define UI_TEST_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Cycle1_Form
{
public:
    QWidget *gridLayoutWidget;
    QGridLayout *gridLayout;
    QSlider *position_slider;
    QPushButton *playpause_button;
    QSlider *volume_slider;
    QLabel *time_label;
    QLabel *duration_label;
    QLabel *time_value;
    QLabel *duration_value;
    QLabel *volume_label;

    void setupUi(QWidget *Cycle1_Form)
    {
        if (Cycle1_Form->objectName().isEmpty())
            Cycle1_Form->setObjectName(QString::fromUtf8("Cycle1_Form"));
        Cycle1_Form->resize(800, 110);
        QSizePolicy sizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Minimum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(Cycle1_Form->sizePolicy().hasHeightForWidth());
        Cycle1_Form->setSizePolicy(sizePolicy);
        Cycle1_Form->setMinimumSize(QSize(780, 110));
        gridLayoutWidget = new QWidget(Cycle1_Form);
        gridLayoutWidget->setObjectName(QString::fromUtf8("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(-1, -1, 801, 101));
        gridLayout = new QGridLayout(gridLayoutWidget);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setSizeConstraint(QLayout::SetMinimumSize);
        gridLayout->setContentsMargins(0, 0, 0, 0);
        position_slider = new QSlider(gridLayoutWidget);
        position_slider->setObjectName(QString::fromUtf8("position_slider"));
        QSizePolicy sizePolicy1(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(position_slider->sizePolicy().hasHeightForWidth());
        position_slider->setSizePolicy(sizePolicy1);
        position_slider->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(position_slider, 0, 1, 1, 1);

        playpause_button = new QPushButton(gridLayoutWidget);
        playpause_button->setObjectName(QString::fromUtf8("playpause_button"));
        playpause_button->setMinimumSize(QSize(90, 20));

        gridLayout->addWidget(playpause_button, 0, 0, 1, 1);

        volume_slider = new QSlider(gridLayoutWidget);
        volume_slider->setObjectName(QString::fromUtf8("volume_slider"));
        sizePolicy1.setHeightForWidth(volume_slider->sizePolicy().hasHeightForWidth());
        volume_slider->setSizePolicy(sizePolicy1);
        volume_slider->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(volume_slider, 1, 1, 1, 1);

        time_label = new QLabel(gridLayoutWidget);
        time_label->setObjectName(QString::fromUtf8("time_label"));
        QSizePolicy sizePolicy2(QSizePolicy::Minimum, QSizePolicy::Minimum);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(time_label->sizePolicy().hasHeightForWidth());
        time_label->setSizePolicy(sizePolicy2);
        time_label->setMaximumSize(QSize(60, 16777215));

        gridLayout->addWidget(time_label, 0, 2, 1, 1);

        duration_label = new QLabel(gridLayoutWidget);
        duration_label->setObjectName(QString::fromUtf8("duration_label"));
        sizePolicy2.setHeightForWidth(duration_label->sizePolicy().hasHeightForWidth());
        duration_label->setSizePolicy(sizePolicy2);
        duration_label->setMaximumSize(QSize(60, 16777215));

        gridLayout->addWidget(duration_label, 1, 2, 1, 1);

        time_value = new QLabel(gridLayoutWidget);
        time_value->setObjectName(QString::fromUtf8("time_value"));
        sizePolicy2.setHeightForWidth(time_value->sizePolicy().hasHeightForWidth());
        time_value->setSizePolicy(sizePolicy2);
        time_value->setMinimumSize(QSize(60, 20));
        time_value->setMaximumSize(QSize(60, 60));

        gridLayout->addWidget(time_value, 0, 3, 1, 1);

        duration_value = new QLabel(gridLayoutWidget);
        duration_value->setObjectName(QString::fromUtf8("duration_value"));
        sizePolicy2.setHeightForWidth(duration_value->sizePolicy().hasHeightForWidth());
        duration_value->setSizePolicy(sizePolicy2);
        duration_value->setMinimumSize(QSize(60, 20));

        gridLayout->addWidget(duration_value, 1, 3, 1, 1);

        volume_label = new QLabel(gridLayoutWidget);
        volume_label->setObjectName(QString::fromUtf8("volume_label"));
        sizePolicy2.setHeightForWidth(volume_label->sizePolicy().hasHeightForWidth());
        volume_label->setSizePolicy(sizePolicy2);
        volume_label->setMinimumSize(QSize(60, 20));
        volume_label->setMaximumSize(QSize(90, 16777215));
        volume_label->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(volume_label, 1, 0, 1, 1);


        retranslateUi(Cycle1_Form);

        QMetaObject::connectSlotsByName(Cycle1_Form);
    } // setupUi

    void retranslateUi(QWidget *Cycle1_Form)
    {
        Cycle1_Form->setWindowTitle(QCoreApplication::translate("Cycle1_Form", "Form", nullptr));
        playpause_button->setText(QCoreApplication::translate("Cycle1_Form", "Pause", nullptr));
        time_label->setText(QCoreApplication::translate("Cycle1_Form", "Time", nullptr));
        duration_label->setText(QCoreApplication::translate("Cycle1_Form", "Duration", nullptr));
        time_value->setText(QCoreApplication::translate("Cycle1_Form", "0:00", nullptr));
        duration_value->setText(QCoreApplication::translate("Cycle1_Form", "0:00", nullptr));
        volume_label->setText(QCoreApplication::translate("Cycle1_Form", "Volume", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Cycle1_Form: public Ui_Cycle1_Form {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TEST_H
