#include "cycle1.h"

void Cycle1::make_connections() {
    //Clicking the play / pause button plays / pauses the video
    connect(ui->playpause_button, SIGNAL(released()), this, SLOT(play_pause()));

    //Resetting the play / pause button when the video changes
    connect(mplayer, SIGNAL(mediaChanged(QMediaContent)), this, SLOT(reset_play_pause(QMediaContent)));

    //Changing the video will change the slider maximum
    connect(mplayer, SIGNAL(durationChanged(qint64)), this, SLOT(setup_slide(qint64)));

    //Changing the slider position will change the video position
    connect(ui->position_slider, SIGNAL(sliderMoved(int)), mplayer, SLOT(set_pos(int)));

    //The slider position will change as the video progresses
    connect(mplayer, SIGNAL(positionChanged(qint64)), this, SLOT(change_slide_pos(qint64)));

    //The time label will change as the video progresses
    connect(mplayer, SIGNAL(positionChanged(qint64)), this, SLOT(change_time_label(qint64)));

    //Setting the duration label when the video changes
    connect(mplayer, SIGNAL(durationChanged(qint64)), this, SLOT(setup_duration_label(qint64)));

    //Changing the volume slider will change the volume
    connect(ui->volume_slider, SIGNAL(valueChanged(int)), mplayer, SLOT(setVolume(int)));
}

// Reset the position of the slider and change its value to the given value
void Cycle1::setup_slide(qint64 value) {
    ui->position_slider->setSliderPosition(0);
    ui->position_slider->setMaximum((int) value);
}

// set the value of the duration label when the video changes
void Cycle1::setup_duration_label(qint64 value) {
    int secs = (int) (value % 60000)/1000;
    int mins = (int) (value - secs*1000) / 60000;
    string duration = to_string(mins) + ":";
    if (secs < 10) {
        duration += "0";
    }
    duration += to_string(secs);
    QString dur = QString::fromStdString(duration);
    ui->duration_value->setText(dur);
}

// change the position of the slider to the given value
void Cycle1::change_slide_pos(qint64 value) {
    ui->position_slider->setSliderPosition((int) value);
}

// change the value of the time label to the one given
void Cycle1::change_time_label(qint64 value) {
    //Convert the given qint64 value in milliseconds to an integer value of seconds
    int secs = (int) (value % 60000)/1000;

    //Convert the given qint64 value in milliseconds to an integer value of minutes
    int mins = (int) (value - secs*1000) / 60000;

    string duration = to_string(mins) + ":";
    if (secs < 10) {
        duration += "0";
    }
    duration += to_string(secs);

    //Convert the string into a QString and set the text of the duration label to it
    QString dur = QString::fromStdString(duration);
    ui->time_value->setText(dur);
}

// handle the play / pause button being clicked
void Cycle1::play_pause() {
    if (ui->playpause_button->text() == "Play"){
        mplayer->play();
        ui->playpause_button->setText("Pause");
    } else {
        mplayer->pause();
        ui->playpause_button->setText("Play");
    }
}

// reset the play / pause button to its original state
void Cycle1::reset_play_pause(QMediaContent) {
    ui->playpause_button->setText("Pause");
}

