#ifndef CYCLE1_H
#define CYCLE1_H

#include "ui_test.h"
#include "the_player.h"
#include <QWidget>
#include <QVideoWidget>
#include <QObject>

class Cycle1 : public QWidget, Ui::Cycle1_Form {

    Q_OBJECT

public:
    Cycle1(ThePlayer *player) {
        ui = new Ui::Cycle1_Form();
        mplayer = player;
        ui->setupUi(this);

        make_connections();
        mplayer->setNotifyInterval(1);
    }

    Ui::Cycle1_Form* getUi() {
        return this->ui;
    }

    void make_connections();

    ~Cycle1() {
        delete ui;
    }

public slots:
    // reset the position of the slider and change its value to the given value
    void setup_slide(qint64);

    // change the position of the slider to the given value
    void change_slide_pos(qint64);

    // set the value of the duration label when the video changes
    void setup_duration_label(qint64);

    // change the value of the time label to the one given
    void change_time_label(qint64);

    // handle the play / pause button being clicked
    void play_pause();

    // reset the play / pause button to its original state
    void reset_play_pause(QMediaContent);

private:
    Ui::Cycle1_Form *ui;

    //holds a pointer to the QMediaPlayer on the main screen
    QMediaPlayer *mplayer;
};

#endif // CYCLE1_H
