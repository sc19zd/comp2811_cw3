#include "secdialog.h"

// create the widgets shown in the dialog window
void Sedialog::createwidget()
{
    z = new QLabel();
    z->setPixmap(QPixmap(":/Warning_icon.png"));
    c = new QLabel("Go into the video editor?");
    c->setFont(QFont("Times", 10, QFont::Bold));
    p = new QPushButton("No");
    p2 = new QPushButton("Yes");
}

// arrange the widgets shown in the second dialog window
void Sedialog::arrangewidget()
{
    QHBoxLayout *t =new QHBoxLayout();
    t->addWidget(p2);
    t->addWidget(p);

    QHBoxLayout *t2 = new QHBoxLayout();
    t2->addWidget(z);
    t2->addWidget(c);

    QVBoxLayout *t3 = new QVBoxLayout();
    t3->addLayout(t2);
    t3->addLayout(t);
    setLayout(t3);

    setWindowTitle("Warning");
    setFixedSize(260,130);

}
