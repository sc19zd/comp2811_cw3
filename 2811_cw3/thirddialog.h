#ifndef THIRDDIALOG_H
#define THIRDDIALOG_H


#include "QDialog"
#include "QHBoxLayout"
#include "QPushButton"
#include "QSlider"
#include "QGroupBox"
#include "QToolButton"
#include "the_player.h"
#include "the_button.h"
#include <QtCore/QDir>
#include <QtCore/QDirIterator>
#include <QImageReader>
#include "QVideoWidget"

class QToolButton;
class QSlider;
class QPushButton;

class Thirddialog : public QDialog
{
private:
    QPalette pal;
    QToolButton *p4;
    QToolButton *p5;
    QToolButton *p6;
    QSlider *brightnessSlider;
    QSlider *contrastSlider;
    QSlider *hueSlider;
    vector<TheButtonInfo> videos;
    QVideoWidget *videoWidget;
    ThePlayer *player;
    QWidget *buttonWidget;
    vector<TheButton*> buttons;
    void createwidget();
    void arrangewidget();
    void makeconnection();


public:
    QPushButton *p3;

    // Set up the third dialog window
    Thirddialog()
    {
        createwidget();
        arrangewidget();
        makeconnection();
    }
};


#endif // THIRDDIALOG_H
